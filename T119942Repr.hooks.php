<?php
/**
 * Hooks for T119942Repr extension
 *
 * @file
 * @ingroup Extensions
 */

class T119942ReprHooks {

	public static function onSkinAfterContent( &$data, Skin $skin ) {
			wfDebugLog( __METHOD__, "SkinAfter hook called" );
			if ( $skin->canUseWikiPage() ) {
				$wikiPage = $skin->getWikiPage();
				wfDebugLog( __METHOD__, '$wikiPage->exists()? ' . var_export( $wikiPage->exists(), true ) );
			}
	}
}
