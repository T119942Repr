<?php

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'T119942Repr' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['T119942Repr'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for T119942Repr extension. Please use wfLoadExtension ' .
		'instead, see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return true;
} else {
	$wgMessagesDirs['T119942Repr'] = __DIR__ . '/i18n';
	$wgHooks['SkinAfterContent'][] = 'T119942ReprHooks::onSkinAfterContent';
}
